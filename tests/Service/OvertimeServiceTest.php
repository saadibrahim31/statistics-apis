<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Dto\OvertimeRequest;
use App\Dto\OvertimeResponse;
use App\Dto\OvertimeScore;
use App\Repository\ReviewRepository;
use App\Service\OvertimeService;
use Doctrine\Common\Collections\ArrayCollection;
use Mockery as m;
use PHPUnit\Framework\TestCase;

/**
 * Class OvertimeServiceTest
 * @package App\Tests\Service
 */
class OvertimeServiceTest extends TestCase
{
    /**
     * @var OvertimeService
     */
    private ?OvertimeService $overtimeService;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $result = [
            [
                "dateGroup" => 4,
                "reviewCount" => 2,
                "averageScore" => 2.4
            ],[
                "dateGroup" => 5,
                "reviewCount" => 1,
                "averageScore" => 1.4
            ],[
                "dateGroup" => 6,
                "reviewCount" => 1,
                "averageScore" => 2.4
            ]
        ];
        $repository = m::mock(ReviewRepository::class)->makePartial();
        $repository->shouldReceive('getAveragePerGroup')->andReturn($result);
        $this->overtimeService = new OvertimeService($repository);
    }

    public function testCanCreateService()
    {
        $this->assertInstanceOf(OvertimeService::class, $this->overtimeService);
    }

    /**
     * @dataProvider dataOvertimeService
     *
     * @param OvertimeRequest $request
     * @param OvertimeResponse $expected
     * @throws \Exception
     */
    public function testOvertimeService(OvertimeRequest $request, OvertimeResponse $expected)
    {
        $result = $this->overtimeService->getAverageScoreForDateRange($request);
        $this->assertEquals($result, $expected);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function dataOvertimeService()
    {
        $collectionDay = new ArrayCollection([
            new OvertimeScore(2.4, 4, 2, OvertimeService::GROUP_BY_DAY),
            new OvertimeScore(1.4, 5, 1, OvertimeService::GROUP_BY_DAY),
            new OvertimeScore(2.4, 6, 1, OvertimeService::GROUP_BY_DAY),
        ]);
        $collectionWeek = new ArrayCollection([
            new OvertimeScore(2.4, 4, 2, OvertimeService::GROUP_BY_WEEK),
            new OvertimeScore(1.4, 5, 1, OvertimeService::GROUP_BY_WEEK),
            new OvertimeScore(2.4, 6, 1, OvertimeService::GROUP_BY_WEEK),
        ]);
        $collectionMonth = new ArrayCollection([
            new OvertimeScore(2.4, 4, 2, OvertimeService::GROUP_BY_MONTH),
            new OvertimeScore(1.4, 5, 1, OvertimeService::GROUP_BY_MONTH),
            new OvertimeScore(2.4, 6, 1, OvertimeService::GROUP_BY_MONTH),
        ]);
        return [
            [
            new OvertimeRequest(
                10,
                '2020-01-03',
                '2020-02-01'
            ),
            new OvertimeResponse($collectionDay)
            ],
            [
                new OvertimeRequest(
                    1000,
                    '2020-01-03',
                    '2020-03-01'
                ),
                new OvertimeResponse($collectionWeek)
            ],
            [
                new OvertimeRequest(
                    10,
                    '2020-01-03',
                    '2020-06-01'
                ),
                new OvertimeResponse($collectionMonth)
            ]
        ];
    }

    protected function tearDown(): void
    {
        $this->overtimeService = null;
    }
}