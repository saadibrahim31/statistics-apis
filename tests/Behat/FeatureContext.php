<?php

declare(strict_types=1);

namespace App\Tests\Behat;

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode;
use Behatch\Context\RestContext;
use Doctrine\ORM\EntityManagerInterface;
use App\Request\Request;

/**
 * Class FeatureContext
 * @package App\Tests\Behat
 */
class FeatureContext extends RestContext
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(
        Request $request,
        EntityManagerInterface $em
    ) {
        parent::__construct($request);
    }

    /**
     * @When a demo scenario sends a request to :arg1
     */
    public function aDemoScenarioSendsARequestTo($arg1)
    {
        throw new PendingException();
    }

    /**
     * @Then the response should be received
     */
    public function theResponseShouldBeReceived()
    {
        throw new PendingException();
    }

    /**
     * @Given /^the JSON matches expected template:$/
     */
    public function theJSONMatchesExpectedTemplate(PyStringNode $string)
    {
        throw new PendingException();
    }
}