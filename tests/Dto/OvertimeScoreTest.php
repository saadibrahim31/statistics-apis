<?php

declare(strict_types=1);

namespace App\Tests\Dto;

use App\Dto\OvertimeRequest;
use Exception;
use PHPUnit\Framework\TestCase;

/**
 * Class OvertimeScoreTest
 * @package App\Tests\Dto
 */
class OvertimeScoreTest extends TestCase
{
    /**
     * @expectedException Exception
     */
    public function testThrowException()
    {
        $this->expectException(Exception::class);
        new OvertimeRequest(10, 'wwess', 'now');
    }

    public function testCanCreate()
    {
        $overtimeRequest = new OvertimeRequest(10, '2020-02-03', 'now');
        $this->assertInstanceOf(OvertimeRequest::class, $overtimeRequest,);
    }
}