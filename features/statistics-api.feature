Feature: Statistics Api
  @statisticsApi
  Scenario: valid request week
    When I add "Content-Type" header equal to "application/json"
    And I add "Accept" header equal to "application/json"
    And I send a "GET" request to "/api/v1/overtime/490/2020-02-01/2020-04-01"
    Then the response status code should be 200
    And the response should be in JSON
    Then the response should be equal to:
    """
{"grouped_collection":[{"average_score":3.5,"date_group":4,"review_count":2,"date_group_type":"WEEK"},{"average_score":2.3,"date_group":5,"review_count":12,"date_group_type":"WEEK"},{"average_score":2.6,"date_group":6,"review_count":9,"date_group_type":"WEEK"},{"average_score":2.0,"date_group":7,"review_count":10,"date_group_type":"WEEK"},{"average_score":3.0,"date_group":8,"review_count":10,"date_group_type":"WEEK"},{"average_score":2.3,"date_group":9,"review_count":8,"date_group_type":"WEEK"},{"average_score":2.4,"date_group":10,"review_count":14,"date_group_type":"WEEK"},{"average_score":2.3,"date_group":11,"review_count":10,"date_group_type":"WEEK"},{"average_score":1.7,"date_group":12,"review_count":11,"date_group_type":"WEEK"},{"average_score":1.8,"date_group":13,"review_count":2,"date_group_type":"WEEK"}]}
    """
  @statisticsApi
  Scenario: valid request day
    When I add "Content-Type" header equal to "application/json"
    And I add "Accept" header equal to "application/json"
    And I send a "GET" request to "/api/v1/overtime/490/2020-02-01/2020-02-20"
    Then the response status code should be 200
    And the response should be in JSON
    Then the response should be equal to:
    """
    {"grouped_collection":[{"average_score":3.5,"date_group":1,"review_count":1,"date_group_type":"DAY"},{"average_score":3.5,"date_group":2,"review_count":1,"date_group_type":"DAY"},{"average_score":3.5,"date_group":3,"review_count":2,"date_group_type":"DAY"},{"average_score":2.5,"date_group":4,"review_count":2,"date_group_type":"DAY"},{"average_score":1.0,"date_group":5,"review_count":1,"date_group_type":"DAY"},{"average_score":3.0,"date_group":6,"review_count":1,"date_group_type":"DAY"},{"average_score":0.3,"date_group":7,"review_count":2,"date_group_type":"DAY"},{"average_score":2.3,"date_group":8,"review_count":3,"date_group_type":"DAY"},{"average_score":4.5,"date_group":9,"review_count":1,"date_group_type":"DAY"},{"average_score":2.5,"date_group":10,"review_count":1,"date_group_type":"DAY"},{"average_score":2.5,"date_group":11,"review_count":2,"date_group_type":"DAY"},{"average_score":2.0,"date_group":12,"review_count":1,"date_group_type":"DAY"},{"average_score":3.3,"date_group":14,"review_count":3,"date_group_type":"DAY"},{"average_score":3.5,"date_group":15,"review_count":1,"date_group_type":"DAY"},{"average_score":0.0,"date_group":16,"review_count":1,"date_group_type":"DAY"},{"average_score":0.5,"date_group":17,"review_count":3,"date_group_type":"DAY"},{"average_score":1.3,"date_group":19,"review_count":2,"date_group_type":"DAY"}]}
    """

  @statisticsApi
  Scenario: valid request month
    When I add "Content-Type" header equal to "application/json"
    And I add "Accept" header equal to "application/json"
    And I send a "GET" request to "/api/v1/overtime/490e0/2020-02-01/2020-07-01"
    Then the response status code should be 200
    And the response should be in JSON
    Then the response should be equal to:
    """
{"grouped_collection":[{"average_score":2.5,"date_group":2,"review_count":43,"date_group_type":"MONTH"},{"average_score":2.1,"date_group":3,"review_count":45,"date_group_type":"MONTH"},{"average_score":2.3,"date_group":4,"review_count":42,"date_group_type":"MONTH"},{"average_score":2.5,"date_group":5,"review_count":41,"date_group_type":"MONTH"},{"average_score":2.2,"date_group":6,"review_count":47,"date_group_type":"MONTH"}]}
    """

  @statisticsApi
  Scenario: bad request
    When I add "Content-Type" header equal to "application/json"
    And I add "Accept" header equal to "application/json"
    And I send a "GET" request to "/api/v1/overtime/490/xxx-xx-xx/2020-04-01"
    Then the response status code should be 400
    And the response should be in JSON
    Then the response should be equal to:
    """
    {"violations":"DateTime::__construct(): Failed to parse time string (xxx-xx-xx) at position 0 (x): The timezone could not be found in the database"}
    """

  @statisticsApi
  Scenario: 404 not found
    When I add "Content-Type" header equal to "application/json"
    And I add "Accept" header equal to "application/json"
    And I send a "GET" request to "/api/v1/overtime/-490/2020-02-01/2020-04-01"
    Then the response status code should be 404