#!/bin/bash

docker-compose -f ./docker/docker-compose.yml up -d --build --remove-orphans
docker-compose -f ./docker/docker-compose.yml exec phpproduct composer install
docker-compose -f ./docker/docker-compose.yml exec phpproduct ./vendor/bin/phpunit tests
docker-compose -f ./docker/docker-compose.yml exec phpproduct vendor/bin/behat