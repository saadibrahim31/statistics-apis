<?php

namespace App\Entity;

use App\Repository\ReviewRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ReviewRepository::class)
 */
class Review
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Hotel", inversedBy="reviews")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank()
     */
    private $hotel;

    /**
     * @ORM\Column(type="float")
     */
    private float $score;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private string $comment;

    /**
     * @ORM\Column(type="datetime")
     */
    private \DateTime $created_date;

    /**
     * Review constructor.
     * @param int|null $id
     * @param Hotel $hotel
     * @param float $score
     * @param string $comment
     * @param null|\DateTime $created_date
     * @throws \Exception
     */
    public function __construct(?int $id, Hotel $hotel, float $score, string $comment, ?\DateTime $created_date = null)
    {
        $this->id = $id;
        $this->hotel = $hotel;
        $this->score = $score;
        $this->comment = $comment;
        $this->created_date = $created_date ?? new \DateTime('now');
    }

    /**
     * @return null|int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getScore(): float
    {
        return $this->score;
    }

    /**
     * @return Hotel
     */
    public function getHotel(): Hotel
    {
        return $this->hotel;
    }

    /**
     * @return string
     */
    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate(): \DateTime
    {
        return $this->created_date;
    }
}