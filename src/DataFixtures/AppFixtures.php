<?php

namespace App\DataFixtures;

use App\Entity\Hotel;
use App\Entity\Review;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

/**
 * Class AppFixtures
 * @package App\DataFixtures
 */
class AppFixtures extends Fixture
{
    /**
     * @var \Faker\Factory
     */
    private $faker;

    /**
     * AppFixtures constructor.
     */
    public function __construct()
    {
        $this->faker = \Faker\Factory::create();
    }

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $this->loadHotel($manager);
        $this->loadReview($manager);
    }

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    private function loadHotel(ObjectManager $manager)
    {
        for ($i = 1; $i <= 1000; $i++) {
            $hotel = new Hotel(null, $this->faker->streetName);
            $this->addReference("hotel_$i", $hotel);

            $manager->persist($hotel);
        }
        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    private function loadReview(ObjectManager $manager)
    {
        $batchSize = 100000;
        for ($i = 0; $i < 1000000; $i++) {
            $hotel = $this->getRandomHotelRefrence();
            $review = new Review(
                null,
                $hotel,
                $this->randFloat(0, 5, 2),
                $this->faker->realText(),
                $this->faker->dateTimeBetween('-2 years', 'now')
            );
            $manager->persist($review);
            if (($i % $batchSize) === 0) {
                $manager->flush();
                $manager->clear(Review::class);
            }
        }
        $manager->flush();
        $manager->clear();
    }

    /**
     * @return Hotel
     */
    private function getRandomHotelRefrence(): Hotel
    {
        return $this->getReference("hotel_" . rand(1, 1000));
    }

    /**
     * @param int $st_num
     * @param int $end_num
     * @param int $mul
     * @return float
     */
    function randFloat(int $st_num=0, int $end_num=1, int $mul = 1000000): float
    {
        if ($st_num>$end_num) return false;
        return mt_rand($st_num*$mul,$end_num*$mul)/$mul;
    }
}
