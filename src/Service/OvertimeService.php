<?php

declare(strict_types=1);

namespace App\Service;

use App\Dto\OvertimeRequest;
use App\Dto\OvertimeResponse;
use App\Dto\OvertimeScore;
use App\Repository\ReviewRepository;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class OvertimeService
 * @package App\Service
 */
class OvertimeService
{
    public const GROUP_BY_DAY = 'DAY';
    public const GROUP_BY_MONTH = 'MONTH';
    public const GROUP_BY_WEEK = 'WEEK';

    /**
     * @var ReviewRepository
     */
    private ReviewRepository $reviewRepository;

    /**
     * OvertimeService constructor.
     * @param ReviewRepository $reviewRepository
     */
    public function __construct(ReviewRepository $reviewRepository)
    {
        $this->reviewRepository = $reviewRepository;
    }

    /**
     * @param OvertimeRequest $overtimeRequest
     * @return OvertimeResponse
     * @throws \Exception
     */
    public function getAverageScoreForDateRange(OvertimeRequest $overtimeRequest): OvertimeResponse
    {
        $groupBy = $this->whichGroupToUse($overtimeRequest);
        $groupedOvertime = $this->reviewRepository->getAveragePerGroup(
            $overtimeRequest->getHotelId(),
            $overtimeRequest->getStartDate(),
            $overtimeRequest->getEndDate(),
            $groupBy
        );
        $result = $this->fillingData($groupedOvertime, $groupBy);

        return new OvertimeResponse($result);
    }

    /**
     * @param array $groupedOvertime
     * @param string $groupBy
     * @return ArrayCollection
     */
    private function fillingData(array $groupedOvertime, string $groupBy): ArrayCollection
    {
        $result = [];
        foreach ($groupedOvertime as $overtimeScore) {
            $result[] = new OvertimeScore((float)$overtimeScore['averageScore'], (int)$overtimeScore['dateGroup'], (int)$overtimeScore['reviewCount'], $groupBy);
        }

        return new ArrayCollection($result);
    }

    /**
     * @param OvertimeRequest $overtimeRequest
     * @return string
     */
    private function whichGroupToUse(OvertimeRequest $overtimeRequest): string
    {
        $interval = $overtimeRequest->getStartDate()->diff($overtimeRequest->getEndDate());
        $result =  self::GROUP_BY_DAY;

        if ($interval->days < 30 ) {
            $result = self::GROUP_BY_DAY;
        } elseif ($interval->days < 90) {
            $result =  self::GROUP_BY_WEEK;
        } elseif ($interval->days >= 90 ) {
            $result =  self::GROUP_BY_MONTH;
        }

        return $result;
    }
}