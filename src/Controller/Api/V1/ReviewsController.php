<?php

declare(strict_types=1);

namespace App\Controller\Api\V1;

use App\Dto\OvertimeRequest;
use App\Exception\ApiProblem;
use App\Exception\ApiProblemException;
use App\Exception\BadRequestException;
use App\Service\OvertimeService;
use App\Validator\InputValidator;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ReviewsController
 * @package App\Controller
 */
class ReviewsController extends AbstractFOSRestController
{
    /**
     * @var OvertimeService
     */
    private OvertimeService $overTimeService;
    /**
     * @var ValidatorInterface
     */
    private ValidatorInterface $validator;

    /**
     * ReviewsController constructor.
     * @param OvertimeService $overTimeService
     * @param ValidatorInterface $validator
     */
    public function __construct(OvertimeService $overTimeService, validatorInterface $validator)
    {
        $this->overTimeService = $overTimeService;
        $this->validator = $validator;
    }

    /**
     * @Rest\Get("/api/v1/overtime/{hotelId}/{startDate}/{endDate}")
     * @SWG\Response(
     *     response=200,
     *     description=" Ok "
     * ),
     * @SWG\Response(
     *     response=400,
     *     description="bad request"
     * ),
     * @SWG\Response(
     *     response=404,
     *     description="Not found"
     * )
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function getAverageScoreForDateRange(Request $request): Response
    {
        $errors = InputValidator::validateGetAverageScoreForDateRange($request, $this->validator);
        if (count($errors) > 0) {
            //return $this->handleView($this->view(["violations" => $errors], 400));
            $this->throwApiProblemValidationException($errors);
        }
        $overtimeRequest = new OvertimeRequest(
            (int)$request->get('hotelId'),
            $request->get('startDate'),
            $request->get('endDate')
        );
        $overtimeResponse = $this->overTimeService->getAverageScoreForDateRange($overtimeRequest);
        $view = $this->view($overtimeResponse, 200);
        if ($overtimeResponse->getGroupedCollection()->isEmpty()) {
            $view = $this->view(null, 404);
        }

        return $this->handleView($view);
    }

    /**
     * @param array $errors
     * @throws \Exception
     */
    private function throwApiProblemValidationException(array $errors)
    {
        $apiProblem = new ApiProblem(
            400,
            ApiProblem::TYPE_VALIDATION_ERROR
        );
        $apiProblem->set('errors', $errors);

        throw new ApiProblemException($apiProblem);
    }
}
