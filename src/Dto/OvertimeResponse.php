<?php

declare(strict_types=1);

namespace App\Dto;

use Doctrine\Common\Collections\Collection;

/**
 * Class OvertimeResponse
 */
class OvertimeResponse
{
    /**
     * @var Collection
     */
    private Collection $groupedCollection;

    /**
     * OvertimeResponse constructor.
     * @param Collection $groupedCollection
     */
    public function __construct(
        Collection $groupedCollection
    ) {
        $this->groupedCollection = $groupedCollection;
    }

    /**
     * @return Collection
     */
    public function getGroupedCollection(): Collection
    {
        return $this->groupedCollection;
    }
}