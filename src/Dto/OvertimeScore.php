<?php

declare(strict_types=1);

namespace App\Dto;

/**
 * Class OvertimeScore
 * @package App\Model
 */
class OvertimeScore
{
    const NUMBER_PRECISION = 1;

    /**
     * @var float
     */
    private float $averageScore;

    /**
     * @var int
     */
    private int $dateGroup;

    /**
     * @var int
     */
    private int $reviewCount;

    /**
     * @var string
     */
    private string $dateGroupType;

    /**
     * OvertimeScore constructor.
     * @param float $averageScore
     * @param int $dateGroup
     * @param int $reviewCount
     * @param string $dateGroupType
     */
    public function __construct(float $averageScore, int $dateGroup, int $reviewCount, string $dateGroupType)
    {
        $this->averageScore = round($averageScore, self::NUMBER_PRECISION);
        $this->dateGroup = $dateGroup;
        $this->dateGroupType = $dateGroupType;
        $this->reviewCount = $reviewCount;
    }

    /**
     * @return float
     */
    public function getAverageScore(): float
    {
        return $this->averageScore;
    }

    /**
     * @return int
     */
    public function getDateGroup(): int
    {
        return $this->dateGroup;
    }

    /**
     * @return int
     */
    public function getReviewCount(): int
    {
        return $this->reviewCount;
    }

    /**
     * @return string
     */
    public function getDateGroupType(): string
    {
        return $this->dateGroupType;
    }
}