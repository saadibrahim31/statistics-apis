<?php

declare(strict_types=1);

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;


/**
 * Class OvertimeResponse
 */
class OvertimeRequest
{
    /**
     * @var int
     */
    private int $hotelId;

    /**
     * @var \DateTime
     */
    private \DateTime $startDate;

    /**
     * @var \DateTime
     */
    private \DateTime $endDate;

    /**
     * OvertimeRequest constructor.
     *
     * @param int $hotelId
     * @param string $startDate
     * @param string $endDate
     * @throws \Exception
     */
    public function __construct(int $hotelId, string $startDate, string $endDate)
    {
        // TODO if flasch date throw exception date is not correct
        $this->hotelId = $hotelId;
        $this->startDate = new \Datetime($startDate);
        $this->endDate = new \Datetime($endDate);
    }

    /**
     * @return int
     */
    public function getHotelId(): int
    {
        return $this->hotelId;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate(): \DateTime
    {
        return $this->startDate;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate(): \DateTime
    {
        return $this->endDate;
    }
}