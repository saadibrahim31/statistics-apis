<?php

namespace App\Repository;

use App\Entity\Review;
use App\Service\OvertimeService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Review|null find($id, $lockMode = null, $lockVersion = null)
 * @method Review|null findOneBy(array $criteria, array $orderBy = null)
 * @method Review[]    findAll()
 * @method Review[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReviewRepository extends ServiceEntityRepository
{
    /**
     * ReviewRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Review::class);
    }

    /**
     * @param int $hotelId
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @param string $groupby
     * @return array
     */
    public function getAveragePerGroup(int $hotelId, \DateTime $startDate, \DateTime $endDate, string $groupby = OvertimeService::GROUP_BY_WEEK): array
    {
        $queryBuilder = $this->createQueryBuilder('r');
        $queryBuilder->select('AVG(r.score) as averageScore, count(r.id)as reviewCount');
        if ($groupby === OvertimeService::GROUP_BY_DAY) {
            $queryBuilder->addSelect('DAY((r.created_date)) as dateGroup');

        }
        if ($groupby === OvertimeService::GROUP_BY_WEEK) {
            $queryBuilder->addSelect('WEEK((r.created_date)) as dateGroup');
        }
        if ($groupby === OvertimeService::GROUP_BY_MONTH) {
            $queryBuilder->addSelect('MONTH((r.created_date)) as dateGroup');
        }
        $queryBuilder->where('r.hotel = :hotelId');
        $queryBuilder->andWhere('r.created_date >= :startDate');
        $queryBuilder->andWhere('r.created_date <= :endDate');
        $queryBuilder->groupBy('dateGroup');
        $queryBuilder->setParameters([
            'hotelId' => $hotelId,
            'startDate' => $startDate,
            'endDate' => $endDate,
        ]);
        $queryBuilder->orderBy('r.created_date', 'ASC');

        return $queryBuilder->getQuery()->getArrayResult();
    }
}
