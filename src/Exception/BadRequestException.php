<?php

declare(strict_types=1);

namespace App\Exception;

/**
 * Class InvalidInputsException
 * @package App\Exception
 */
class BadRequestException extends \Exception
{
    protected $message = "Bad request";
}