<?php

declare(strict_types=1);

namespace App\Validator;

use App\Exception\ApiProblem;
use App\Exception\ApiProblemException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ValidateInput
 * @package App\Validator
 */
class InputValidator
{
    /**
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return array
     * @throws \Exception
     */
    public static function validateGetAverageScoreForDateRange(Request $request, validatorInterface $validator): array
    {
        $hotelId = $request->get('hotelId');
        $startDate = $request->get('startDate');
        $endDate = $request->get('endDate');
        $inputs = ['hotelId' => (int)$hotelId, 'startDate' => $startDate, 'endDate' => $endDate];

        $constraints = new Assert\Collection([
            'hotelId' => [
                new Assert\Type("integer", "The value {{ value }} is not a valid {{ type }}.")
            ], 'startDate' => [
                new Assert\DateTime("Y-m-d H:i:s", "The value {{ value }} is not a valid Y-m-d H:i:s."),
                new Assert\notBlank
            ], 'endDate' => [
                new Assert\DateTime("Y-m-d H:i:s", "The value {{ value }} is not a valid Y-m-d H:i:s."),
                new Assert\notBlank
            ]
        ]);

        $violations = $validator->validate($inputs, $constraints);
        $errorMessages = [];
        if (count($violations) > 0) {
            $accessor = PropertyAccess::createPropertyAccessor();
            foreach ($violations as $violation) {
                $accessor->setValue($errorMessages,
                    $violation->getPropertyPath(),
                    $violation->getMessage());
            }
        }

        return $errorMessages;
    }

    /**
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return array
     * @throws \Exception
     */
    public static function validateJSONRequest(Request $request, validatorInterface $validator): array
    {
        $data = json_decode($request->getContent(), true);
        if ($data === null) {
            $apiProblem = new ApiProblem(400, ApiProblem::TYPE_INVALID_REQUEST_BODY_FORMAT);

            throw new ApiProblemException($apiProblem);
        }
    }
}