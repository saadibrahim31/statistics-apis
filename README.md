
## How To

- create symfony 
You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. To run project inside root folder.

```
./install.sh
```
OR
```
docker-compose -f ./docker/docker-compose.yml up -d --build --remove-orphans

docker-compose -f ./docker/docker-compose.yml exec phpproduct composer install
```

2. To run tests
unit tests
```
docker-compose -f ./docker/docker-compose.yml exec phpproduct ./vendor/bin/phpunit tests
```
functional tests
```
docker-compose -f ./docker/docker-compose.yml exec phpproduct vendor/bin/behat
```

3. For Api documentations

```
http://localhost/api/doc
```

#when want to stop project

```
./destroy_server.sh
```
---
#!/bin/bash

docker-compose -f ./docker/docker-compose.yml up -d --build --remove-orphans
docker-compose -f ./docker/docker-compose.yml exec phpproduct composer install
docker-compose -f ./docker/docker-compose.yml exec phpproduct ./vendor/bin/phpunit tests
docker-compose -f ./docker/docker-compose.yml exec phpproduct vendor/bin/behat
## Result of calling the api

```
http://localhost/api/v1/overtime/2690/2020-02-01%2023:59:59/2020-03-01%2023:59:59 method={"GET"}
```
http code: 200
```json
{
    "grouped_collection": [
        {
            "average_score": 2.0,
            "date_group": 5,
            "review_count": 1,
            "date_group_type": "DAY"
        },
        {
            "average_score": 5.0,
            "date_group": 8,
            "review_count": 1,
            "date_group_type": "DAY"
        },
        {
            "average_score": 0.5,
            "date_group": 14,
            "review_count": 1,
            "date_group_type": "DAY"
        },
        {
            "average_score": 3.5,
            "date_group": 21,
            "review_count": 1,
            "date_group_type": "DAY"
        },
        {
            "average_score": 3.0,
            "date_group": 29,
            "review_count": 1,
            "date_group_type": "DAY"
        }
    ]
}
```

```
http://localhost/api/v1/overtime/2690/2020-02-01%2023:59:59/2020-06-01%2023:59:59 method={"GET"}
```

http code: 200
```json
{
    "grouped_collection": [
        {
            "average_score": 2.8,
            "date_group": 2,
            "review_count": 5,
            "date_group_type": "MONTH"
        },
        {
            "average_score": 2.4,
            "date_group": 3,
            "review_count": 7,
            "date_group_type": "MONTH"
        },
        {
            "average_score": 2.3,
            "date_group": 4,
            "review_count": 3,
            "date_group_type": "MONTH"
        },
        {
            "average_score": 3.0,
            "date_group": 5,
            "review_count": 4,
            "date_group_type": "MONTH"
        },
        {
            "average_score": 1.0,
            "date_group": 6,
            "review_count": 1,
            "date_group_type": "MONTH"
        }
    ]
}
```

```
http://localhost/api/v1/overtime/2690/2020-02-01%2023:59:59/2020-04-01%2023:59:59 method={"GET"}
```

http code: 200
```json
{
    "grouped_collection": [
        {
            "average_score": 3.5,
            "date_group": 5,
            "review_count": 2,
            "date_group_type": "WEEK"
        },
        {
            "average_score": 0.5,
            "date_group": 6,
            "review_count": 1,
            "date_group_type": "WEEK"
        },
        {
            "average_score": 3.5,
            "date_group": 7,
            "review_count": 1,
            "date_group_type": "WEEK"
        },
        {
            "average_score": 3.0,
            "date_group": 8,
            "review_count": 1,
            "date_group_type": "WEEK"
        },
        {
            "average_score": 2.5,
            "date_group": 9,
            "review_count": 2,
            "date_group_type": "WEEK"
        },
        {
            "average_score": 4.0,
            "date_group": 10,
            "review_count": 1,
            "date_group_type": "WEEK"
        },
        {
            "average_score": 1.0,
            "date_group": 11,
            "review_count": 1,
            "date_group_type": "WEEK"
        },
        {
            "average_score": 2.2,
            "date_group": 12,
            "review_count": 3,
            "date_group_type": "WEEK"
        }
    ]
}
```

```
http://localhost/api/v1/overtime/2700/2020-02-01%2023:59:59e/2020-03-01%2023:59:59w
```

http code: 400
```json
{"errors":{"startDate":"The value \u00222020-02-01 23:59:59w\u0022 is not a valid Y-m-d H:i:s.","endDate":"The value \u00222020-03-01 23:59:59w\u0022 is not a valid Y-m-d H:i:s."},"status":400,"type":"http:\/\/localhost:8000\/docs\/errors#validation_error","title":"There was a validation error"}```


```
http://localhost/api/v1/overtime/204/2020-02-01%2023:59:59/2020-03-01%2023:59:59
```

http code: 404

